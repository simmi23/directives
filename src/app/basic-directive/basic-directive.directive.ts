import {Directive, ElementRef, Input, OnInit} from '@angular/core';

@Directive({
  // use this as an attribute
  selector: '[appBasicDirective]'
})
export class BasicDirective implements OnInit {
  @Input('appBasicDirective') color;
  constructor(private elementRef: ElementRef) {}
  ngOnInit(): void {
    this.elementRef.nativeElement.style.backgroundColor = 'blue';
    this.elementRef.nativeElement.style.color = 'white';
    this.elementRef.nativeElement.style.backgroundColor = this.color;
  }
}
