import {Directive, ElementRef, HostBinding, HostListener, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[appBetterDirective]'
})
export class BetterDirective implements OnInit {
  // Decorator imported from angular/core-> @HostBinding('style.backgroundColor') variableName = value;
  // style.backgroundColor -> to which property of hosting element we want to bind
  // variableName -> property  whose value is important
  // to make it dynamic ->to take color at run time,we use as below @Input
  @Input() defaultColor = 'transparent';
  @Input() highlightedColor = 'blue';
 @HostBinding('style.color') Color: string;
  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }
  // When angular is not running on browsers then angular will not access DOM so the
  // basic directive we created will not work in this case
  // so it is always a good practise to use renderer methods while creating our own directives
  ngOnInit(): void {
    // this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'yellow');
    // this.defaultColor -> initialise here bcoz it will take updated color not transparent as define at start
    this.Color = this.defaultColor;
  }
  // -----------Events on some activity like hover, mouseover,mouseleave,etc
  // use @HostListener As decorator and pass argument as some event
   @HostListener('mouseenter') onMouseOver(data: Event) {
     this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'yellow');
     this.Color = this.highlightedColor;
   }
    @HostListener('mouseleave') onMouseLeave(data: Event) {
      this.renderer.setStyle(this.elementRef.nativeElement, 'backgroundColor', 'blue');
      this.Color = this.defaultColor;
    }
}
