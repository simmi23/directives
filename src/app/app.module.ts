import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BasicDirective} from './basic-directive/basic-directive.directive';
import {BetterDirective} from './better-directive/better-directive.directive';
import { UserCreatedStruDirectiveDirective } from './user-created-stru-directive.directive';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    BasicDirective,
    BetterDirective,
    UserCreatedStruDirectiveDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
