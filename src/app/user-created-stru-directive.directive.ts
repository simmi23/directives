import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[appUserCreatedStruDirective]'
})
export class UserCreatedStruDirectiveDirective {
  // selector and property name must be same i.e. appUserCreatedStruDirective
  @Input() set appUserCreatedStruDirective(condition: boolean) {
    if (!condition) {
      this.vcRef.createEmbeddedView(this.templateRef);
    } else {
      this.vcRef.clear();
    }
  }
  constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) { }

}
