import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'directives';
  onlyodd = false;
  OddNumbers = [1, 3, 5];
  evenNumbers = [2, 4];
  value1;
  onValueChanged(changevalue: HTMLInputElement) {
    this.value1 = changevalue.value;
    print();
  }
}
